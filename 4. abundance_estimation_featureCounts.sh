#!/bin/bash
#SBATCH --job-name="test"
#SBATCH --nodes=12
#SBATCH --cpus-per-task=1
#SBATCH --time=0-12:00
#SBATCH -o fc.o%j

#load necessary module
module load SUBREAD

#for all the sorted and indexed bam files in the folder do abundance estimation and produce ONE count text file for every .bam file
for sortedbam in $(ls *.bam)
do
name=${sortedbam##*/} #removes the filepath name up until the / of the file name
base=${name%.bam}
echo $base && featureCounts -T 12 -g gene_name -a ~/refs/Homo_sapiens.GRCh38.96.chr.gtf -o ~/rnaseq/counts/"${base}".txt $sortedbam
done
