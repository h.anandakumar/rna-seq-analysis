#!/bin/bash
#SBATCH --job-name="test"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=12
#SBATCH --time=0-24:00
#SBATCH -o 664.o%j

#load necessary module

module load SAMTOOLS


#for all the SORTED bam files in the current folder, index it using samtools index function
for bam in $(ls *.bam)
do
echo "Indexing: "$bam
samtools index $bam $bam".bai"
done


