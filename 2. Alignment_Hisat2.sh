#!/bin/bash
#SBATCH --job-name="test"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=12
#SBATCH --time=0-24:00
#SBATCH -o 664.o%j


#load require modules
module load HISAT2
module load SAMTOOLS
module load EDIRECT


#a .txt file containing all the file names 
samples=/usr/users/hananda/rnaseq/scripts/664.txt

for SAMPLE in $(cat $samples)
do
#output directory of where the aligned bam files should go 
dir2="/usr/users/hananda/rnaseq/bam/664"
#echoes the file being processed ; split the process into 12 threads ; 
echo $SAMPLE && hisat2 -p 12 -x /usr/users/hananda/refs/grch38/genome -U /usr/users/hananda/rawdata/RNASequencing/Data_03_2018/664/${SAMPLE}.fastq.gz | samtools sort > ${dir2}/${SAMPLE}.bam
done
