#!/bin/bash
#SBATCH --job-name="fastqc"
#SBATCH --nodes=1
#SBATCH --cpus-per-task=24
#SBATCH --time=2-00:00
#SBATCH -o fc.o%j
#SBATCH -e fc.o%j.err

module load FASTQC

#output file location
output=~/rnaseq/FASTQCresults/


#a for loop that loops through the folder 'raw_reads' and passes all files with .fastq.gz extension 
for file in  ~/rawdata/RNASequencing/raw_reads/*.fastq.gz
do
name=${file##*/} #removes the filepath name up until the / of the file name
base=${name%.fastq.gz}

echo $base && fastqc -t 24 -o ${output} $file

#fastqc -t 6 -o ${output} ${file};
done
